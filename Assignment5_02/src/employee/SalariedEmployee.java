package employee;

public class SalariedEmployee extends Employee{
	
	private double weeklySalary;
	
	public SalariedEmployee() {
		this("","",0);
	}
	
	public SalariedEmployee(String firstName,String lastName,double salary) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.SSN = counter++;
		this.weeklySalary = salary;
	}

	@Override
	public double earnings() {
		return this.weeklySalary;
	}

	@Override
	public String toString() {
		return "Salaried employee: "+this.firstName+" "+this.lastName+
				"\r\nSocial security number: "+this.SSN+
				"\r\nWeekly salary: "+this.weeklySalary;
	}

}
