package employee;

public class HourlyEmployee extends Employee{
	
	private double wage;
	private double hours;
	
	public HourlyEmployee() {
		this("","",0,0);
	}
	
	public HourlyEmployee(String firstName,String lastName,double wage,double hours) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.SSN = counter++;
		this.wage = wage;
		this.hours = hours;
	}

	@Override
	public double earnings() {
		if(this.hours <= 40)
			return this.wage*this.hours;
		else
			return 40*this.wage + (this.hours-40)*this.wage*1.5;
	}

	@Override
	public String toString() {
		return "Hourly employee: "+this.firstName+" "+this.lastName+
				"\r\nSocial security number: "+this.SSN+
				"\r\nHourly wage: "+this.wage+
				"Hours worked: "+this.hours;
	}

}
