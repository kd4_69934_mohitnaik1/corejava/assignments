package employee;

public class BasePlusCommisionEmployee extends Employee{
	
	
	private double baseSalary;
	private double commissionRate;
	private double grossSales;
	
	public BasePlusCommisionEmployee() {
		this("","",0,0);
	}
	
	public BasePlusCommisionEmployee(String firstName,String lastName,double commission,double grossSales) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.SSN = counter++;
		this.commissionRate = commission;
		this.grossSales = grossSales;
	}

	@Override
	public double earnings() {
		return (this.commissionRate*this.grossSales) + this.baseSalary;
	}

	@Override
	public String toString() {
		return "Base salaried employee: "+this.firstName+" "+this.lastName+
				"\r\nSocial security number: "+this.SSN+
				"\r\nGross sales: "+this.grossSales+
				"\r\nCommision rate: "+this.commissionRate+
				"\r\nBase salary: "+this.baseSalary;
	}

}
