package employee;

public class CommissionEmployee extends Employee {
	
	private double commissionRate;
	private double grossSales;
//	private double baseSalary;
	
	
	@Override
	public double earnings() {
		return this.commissionRate*this.grossSales;
	}

	public CommissionEmployee() {
		this("","",0,0);
	}
	
	public CommissionEmployee(String firstName,String lastName,double commission,double grossSales) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.SSN = counter++;
		this.commissionRate = commission;
		this.grossSales = grossSales;
	}

	@Override
	public String toString() {
		return "Commision employee: "+this.firstName+" "+this.lastName+
				"\r\nSocial security number: "+this.SSN+
				"\r\nHourly wage: "+this.grossSales+
				"\r\nHours worked: "+this.commissionRate;
	}

}
