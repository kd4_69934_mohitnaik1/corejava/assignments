package employee;

public abstract class Employee {
	
	protected String firstName;
	protected String lastName;
	protected long SSN;
	
	protected static long counter;
	
	static {
		counter = 100000;
	}
	
	abstract public double earnings();
	abstract public String toString();

}
