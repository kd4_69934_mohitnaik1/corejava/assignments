package test;

import employee.SalariedEmployee;
import employee.BasePlusCommisionEmployee;
import employee.CommissionEmployee;
import employee.Employee;
import employee.HourlyEmployee;

public class Test {


	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		Employee e1 = new SalariedEmployee("Shiv","Jadhav",100000);
		System.out.println(e1);
		System.out.println(e1.earnings());
		
		Employee e2 = new HourlyEmployee("Ashutosh","Khadse",2000000,5);
		System.out.println(e2);
		System.out.println(e2.earnings());
		
		Employee e3 = new CommissionEmployee("Mohit","Naik",20000,2000);
		System.out.println(e3);
		System.out.println(e3.earnings());
		
		Employee e4 = new BasePlusCommisionEmployee("Rohit","Naik",20,100);
		System.out.println(e4);
		System.out.println(e4.earnings());
		
	}

}
