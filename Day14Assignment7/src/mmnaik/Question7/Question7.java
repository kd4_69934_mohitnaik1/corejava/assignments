package mmnaik.Question7;

import java.util.stream.Stream;

public class Question7 {

	public static void main(String[] args) {
		
		Stream<Double> strm = Stream.generate(()->Math.random())
								.limit(25);
		strm.forEach( s -> System.out.println(s));
		
		System.out.println("*********");
		Stream<Double> strm2 =Stream.generate(()->Math.random())
				.limit(25);
				strm2.map(n -> (int)(n*100)).forEach( s -> System.out.println(s));
		
				System.out.println("*********");
				
		Stream<Double> strm3 = Stream.generate(()->Math.random()).limit(25);
				Integer num = strm3.map(n -> (int)(n*100))
				.filter(i-> i%2==1)
				.min((x,y)->x-y)
				.get();
				
				System.out.println(num);
				
				System.out.println("**********");
		Stream<Double> strm4 =	Stream.generate(()->Math.random()).limit(25);
								strm4.map(n -> (int)(n*100))
								.filter(i-> i%2==1)
								.sorted((x,y)->x-y)
								.limit(5)
								.forEach(s->System.out.println(s));
//							
//					strm4.forEach(s -> System.out.println(s));
		
	}

}
