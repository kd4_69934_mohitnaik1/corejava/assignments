package mmnaik.genericSort;

import java.util.Comparator;
import java.util.Scanner;


class CustomComparator implements Comparator<Float>{
		

		@Override
		public int compare(Float n1, Float n2) {
			// TODO Auto-generated method stub
						if(n1 < n2)
							return -1;
						if(n1 == n2)
							return 0;
						else
							return 1;
		}
	}

public class GenericSort{
	
	public GenericSort() {
		
	}
	
	public static <T extends Number> void selectionSort(T[] arr,Comparator c) {
		for(int i=0;i<arr.length;i++) {
			for(int j=i+1;j<arr.length;j++) {
				if(c.compare(arr[i],arr[j]) > 0)
				{
					T temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}

	public static void main(String[] args) {
		
		Float[] arr = new Float[6];
		
		Scanner sc = new Scanner(System.in);
		
		for(int i=0;i<6;i++)
			arr[i] = sc.nextFloat();
			
		selectionSort(arr,new CustomComparator());
		for(int i=0;i<6;i++)
			System.out.print(arr[i]+" ");
	}


}
