package mmnaik.question1;

import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

class Book {
	
	private String isbn;
	private double price;
	private String authorName;
	private int quantity;
	
	public Book(String isbn, double price, String authorName, int quantity) {
		super();
		this.isbn = isbn;
		this.price = price;
		this.authorName = authorName;
		this.quantity = quantity;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof Book))
			return false;
		Book other = (Book)obj;
		if(this.isbn == other.isbn)
			return true;
		return false;
		
	}

	@Override
	public String toString() {
		return "Book [isbn=" + isbn + ", price=" + price + ", authorName=" + authorName + ", quantity=" + quantity
				+ "]";
	}
	
	
	
}

public class Question1 {
	
	public static void switchCase(List<Book> l1,int choice) {
		
		switch(choice) {
		
			case 1:
				l1.add(new Book("Fire and Blood",500.0,"Georgr Martin",20));
				break;
			
			case 2:
				l1.forEach(new Consumer<Book>() {
					public void accept(Book b) {
						System.out.println(b);
					}
				});
				
//				l1.forEach();
				break;
			
			case 3:
				ListIterator<Book> itr = l1.listIterator(l1.size()) ;
				while(itr.hasPrevious()) {
					Book b = itr.previous();
					System.out.println(b);
				}
				break;
			
			case 4:
				Book b1 = new Book("Percy Jackson",0,"hhbkj",30);
				System.out.println(l1.indexOf(b1));
				break;
			
			case 5:
				l1.remove(3);
				break;
			
			case 6: 
//				l1.sort( new Comparator<Book>() {
//					public int compare(Book b1,Book b2) {
//						if(b1.getPrice()<b2.getPrice()) return +1;
//						if(b1.getPrice()>b2.getPrice()) return -1;
//						else return 0;
//					}
//				});
				
				l1.sort((x,y) -> (int)Math.signum(x.getPrice()-y.getPrice()));
				break;
				
			case 7:
				System.out.println("Enter index: ");
				int n = new Scanner(System.in).nextInt();
				Book b2 = new Book("",0.0,"",0);
				l1.remove(n);
				l1.add(n,b2);
				break;
			
			case 8:
//				l1.removeIf(new Predicate<Book>() {
//					
//				public boolean test(Book b) { 
//					if(b.getPrice() < 200) return true;
//					return false;
//				}
//				});
//				
				l1.removeIf(b -> b.getPrice() < 200);
				break;
			
			case 0:
				System.out.println("Exiting...");
				break;
		}
		
	}
	
	public static int menu(Scanner sc) {
		System.out.println("1. Add new Book");
		System.out.println("2. Display all books");
		System.out.println("3. Display all books in reverse order");
		System.out.println("4. Search a Book");
		System.out.println("5. Delete a Book");
		System.out.println("6. Sort all books by price");
		System.out.println("7. Replace a Book");
		System.out.println("8. Remove all books");
		System.out.println("0. Exit");
		
		return sc.nextInt();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Book> l1 = new ArrayList<>();
		l1.add(new Book("Percy Jackson",700.0,"Rick Ryden",30));
		l1.add(new Book("Harry Potter",500.0,"Georgr Martin",20));
		l1.add(new Book("Immortal of Meluhas",500.0,"Amish Tripathi",20));
		l1.add(new Book("Fire",500.0,"Georgr Martin",20));
		l1.add(new Book("THe fault in our stars",500.0,"John green",20));
		
		Scanner sc = new Scanner(System.in);
		
		int choice = menu(sc);
		
		while(choice!=0) {
			
			switchCase(l1,choice);
			
			choice = menu(sc);
			
		}
		
		
	}

}
