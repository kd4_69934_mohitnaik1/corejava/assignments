import java.util.Scanner;

class Numcheck{
	public static void palindrome(int num) {
		int temp = num;
		int ans = 0;
		while(temp!=0)
		{
			ans = ans*10 + temp%10;
			temp /= 10;
		}
		
		if(ans == num)
			System.out.println(num+" is Palindrome number");
		else
			System.out.println(num+" is not Palindrome");
	}
	public static void armstrong(int num) {
		int temp = num;
		int ans = 0;
		int digits=0;
		while(temp!=0) {
			temp/=10;
			digits++;
		}
		temp = num;
		while(temp!=0)
		{
			ans += Math.pow(temp%10,digits);
			temp /= 10;
		}
		
		if(ans == num)
			System.out.println(num+" is Armstrong number");
		else
			System.out.println(num+" is not Armstrong");
	}
}
public class program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter a number ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println("1. Palindrome");
		System.out.println("2. Armstrong");
		System.out.println("Enter choice");
		int choice = sc.nextInt();
		
		switch(choice)
		{
		case 1:
			Numcheck.palindrome(num);
			break;
		case 2:
			Numcheck.armstrong(num);
			break;
		default:
			break;
		}
	}

}
