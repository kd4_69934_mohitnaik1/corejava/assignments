package count_words;

import java.util.Scanner;

public class CountWords {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the String");
		Scanner sc = new Scanner(System.in);
		
		String s = sc.nextLine();
		String[] s1 = s.split("\\s+");
		int count = 0;
		
		for(String str:s1)
			count++;
		System.out.println("No. of words are "+count);
	}

}

