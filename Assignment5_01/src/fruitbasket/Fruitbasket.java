package fruitbasket;

import java.util.Scanner;

import com.app.fruit.Apple;
import com.app.fruit.Fruit;
import com.app.fruit.Mango;
import com.app.fruit.Orange;

public class Fruitbasket {
	
	@SuppressWarnings("resource")
	public static int menu() {
		
		System.out.println("1. Add Mango");
		System.out.println("2. Add Orange");
		System.out.println("3. Add Apple");
		System.out.println("4. Display all names of fruits in fruitbasket");
		System.out.println("5. Display Fruit state");
		System.out.println("6. Display tastes of all stale fruits");
		System.out.println("7. Mark fruit a stale");
		System.out.println("8. Mark all sour fruits stale");
		System.out.println("10. Exit");
		return new Scanner(System.in).nextInt();
	}

	public static void main(String []args)
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter size of FruitBasket");
		int n = sc.nextInt();
		int counter = 0;
		
		Fruit []fruits = new Fruit[n];
		
		int choice = menu();
		
		while(counter<=n && choice!=10)
		{
			switch(choice) {
			
					case 1:
						fruits[counter++] = new Mango();
						break;
					case 2:
						fruits[counter++] = new Orange();
						break;
					case 3:
						fruits[counter++] = new Apple();
						break;
						
					case 4:
						for(Fruit fruit:fruits) {
							if(fruit!=null)
								System.out.println(fruit.getName());
						}
						break;
						
					case 5:
						for(Fruit fruit:fruits) {
							if(fruit!=null && fruit.getIsFresh())
								System.out.println(fruit);
								fruit.taste();
						}
						break;
					
					case 6:
						for(Fruit fruit:fruits) {
							if(!fruit.getIsFresh())
								System.out.println(fruit.taste());
						}
						
						break;
					
					case 7:
						System.out.println("Enter index of fruit you want to mark stale = ");
						int ind = sc.nextInt();
						if(fruits[ind]!= null && ind>=0 && ind<n)
							fruits[ind].setIsFresh();
						else
							System.out.println("Invalid index");
						break;
					
					case 8:
						for(Fruit fruit:fruits) {
							if(fruit != null) {
								if(fruit.taste() == "sour")
									fruit.setIsFresh();
							}
							
						}
						break;
						
					case 10:
						System.out.println("Exiting...");
						break;
						
					default:
						break;
			}
			choice = menu();
		}
		
		
		
		
	}
}
