package com.app.fruit;

public class Mango extends Fruit{
	
	public Mango() {
		this.color = "yellow";
		this.weight = 0.2;
		this.isFresh = true;
		this.name = "Mango";
	}
	
	@Override
	public String toString() {
		return  "This Fruit is "+this.name+".It is "+this.color+". Its weight is "+this.weight+".";
	}
	
	@Override
	public String taste() {
		return "sweet";
	}
}
