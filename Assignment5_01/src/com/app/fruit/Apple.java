package com.app.fruit;

public class Apple extends Fruit{
	
	public Apple() {
		this.color = "red";
		this.weight = 0.2;
		this.isFresh = true;
		this.name = "Apple";
	}
	
	@Override
	public String toString() {
		return  "This Fruit is "+this.name+".It is "+this.color+". Its weight is "+this.weight+".";
	}
	
	@Override
	public String taste() {
		return "sweet and sour";
	}
	
}
