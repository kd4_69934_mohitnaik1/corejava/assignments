package com.app.fruit;

public class Fruit {
	
	protected String color;
	protected String name;
	protected double weight;
	protected boolean isFresh = true;
	
	public String taste() {
		return "No specific taste";
	}
	
	public String getName() {
		return this.name;
	}
	
	public boolean getIsFresh() {
		return this.isFresh;
	}
	
	public void setIsFresh() {
		this.isFresh = false;
	}

}
