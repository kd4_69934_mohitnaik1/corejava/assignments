package mmnaik.Question2;

import java.util.Stack;

public class Question2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String [] arr = new String[] {
				"Ashu","Pratik","mohit","Shiv"
		};
		
		Stack<String> stk = new Stack<>();
		
		for(int i=0;i<arr.length;i++) {
			stk.push(arr[i]);
			arr[i] = "";
		}
		System.out.println(stk);
		
		int i = 0;
		while(!stk.empty()) {
			arr[i] = stk.pop();
			i++;
		}
//		System.out.println(arr);
		
		for(String s:arr) {
			System.out.print(s+" ");
		}
	}

}
