package mmnaik.assignment1;

interface Emp{
	double getSal();
	default double calcIncentives() {
		return 0.0;
		
	}
	
	public static double calcTotalIncome(Emp[] arr) {
		double total = 0;
		for(int i=0;i<arr.length;i++) {
			total += (arr[i].getSal()+arr[i].calcIncentives());
		}
		return total;
	}
}

class Manager implements Emp{
	
	private double basicSalary;
	private double dearanceAllowance;
	
	
	
	public Manager(double basicSalary, double dearanceAllowance) {
		super();
		this.basicSalary = basicSalary;
		this.dearanceAllowance = dearanceAllowance;
	}

	@Override
	public double getSal() {
		return this.basicSalary+this.dearanceAllowance;
	}
	
	public double calcIncentives() {
		return 0.2*this.basicSalary;
	}
}

class Labor implements Emp{
	private int hours;
	private double rate;
	
	
	public Labor(int hours, double rate) {
		super();
		this.hours = hours;
		this.rate = rate;
	}

	@Override
	public double getSal() {
		return this.hours*this.rate;
	}
	
	public double calcIncentives() {
		if(this.hours>300)
			return 0.05*this.getSal();
		return 0.0;
	}
}

class Clerk implements Emp{
	private double salary;
	
	
	public Clerk(double salary) {
		super();
		this.salary = salary;
	}


	@Override
	public double getSal() {
		return this.salary;
	}
	
}
public class Assignment1 {

	public static void main(String[] args) {
		
		Emp[] arr = new Emp[] {
				new Manager(200000,3000),
				new Manager(200000,3000),
				new Clerk(40000),
				new Labor(400,100)
		};
		
		System.out.println(Emp.calcTotalIncome(arr));
		
		
		

	}

}
