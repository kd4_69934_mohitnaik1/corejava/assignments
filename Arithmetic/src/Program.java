import java.util.Scanner;

class Arithmetic{
	
	public static void add(int num1,int num2) {
		System.out.println("Addition is "+(num1+num2));
	}
	
	public static void substract(int num1,int num2) {
		System.out.println("Substraction is "+(num1-num2));
	}
	
	public static void multiply(int num1,int num2) {
		System.out.println("Multiplication is "+(num1*num2));
	}
	
	public static void divide(int num1,int num2) {
		System.out.println("Division is "+(num1/num2));
	}
	
	
}
public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("1. Add");
		System.out.println("2. Substract");
		System.out.println("3. Multiply");
		System.out.println("4. Division");
		System.out.println("0. Exit");
		
		Scanner sc = new Scanner(System.in);
		
		int choice,num1=0,num2=0;

		do {
			System.out.println("Enter your choice = ");
			choice = sc.nextInt();
			
			if(choice!=0)
			{
				System.out.println("Enter two numbers = ");
				num1 = sc.nextInt();
				num2 = sc.nextInt();
			}
			

			switch(choice)
			{
			
			case 1:
				Arithmetic.add(num1,num2);
				break;
			case 2:
				Arithmetic.substract(num1,num2);
				break;
			case 3:
				Arithmetic.multiply(num1,num2);
				break;
			case 4:
				Arithmetic.divide(num1,num2);
				break;
			case 0:
				break;
			default:
				System.out.println("Wrong input Try again!");
			}
		}while(choice!=0);
		
		System.out.println("Loop exited");
		
		
	}

}
