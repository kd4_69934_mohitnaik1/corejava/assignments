package mmnaik.Question5;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


class Book implements Comparable<Book>{
	
	private String isbn;
	private double price;
	private String authorName;
	private int quantity;
	
	public Book(String isbn, double price, String authorName, int quantity) {
		super();
		this.isbn = isbn;
		this.price = price;
		this.authorName = authorName;
		this.quantity = quantity;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof Book))
			return false;
		Book other = (Book)obj;
		if(this.isbn == other.isbn)
			return true;
		return false;
		
	}
	
	@Override
	public int hashCode() {
		
		return this.isbn.hashCode();
	}

	@Override
	public String toString() {
		return "Book [isbn=" + isbn + ", price=" + price + ", authorName=" + authorName + ", quantity=" + quantity
				+ "]";
	}

	@Override
	public int compareTo(Book b) {
			
		return this.getIsbn().compareTo(b.getIsbn());
	}
	
}

public class Question5 {

	public static void main(String[] args) {
		
		Map<String,Book> mp = new HashMap<>();
		
		mp.put("5676565",new Book("5676565",344.0,"fdkfj frfr",20));
		mp.put("34434435",new Book("34434435",1000.0,"jfskfjie sff",10));
		mp.put("21324356",new Book("21324356",343.0,"fdkfj frfee",5));
		mp.put("788654545",new Book("788654545",566.0,"fdkfj frfr",60));
		mp.put("5676565",new Book("5676565",344.0,"fdkfj frfr",20));
		
//		mp.forEach((k,v) -> System.out.println(k+": "+v));
		Scanner sc = new Scanner(System.in);
		String isbn = sc.next();
		System.out.println(mp.get(isbn));
	}

}
