package tester;

import java.util.Scanner;
import com.app.geometry.Point2D;

public class TestPoint {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter x and y for point1");
		int x = sc.nextInt();
		int y = sc.nextInt();
		
		Point2D p1 = new Point2D(x,y);
		
		System.out.println("Enter x and y for point2");
		x = sc.nextInt();
		y = sc.nextInt();
		
		Point2D p2 = new Point2D(x,y);
		
		System.out.println(p1.getDetails());
		System.out.println(p2.getDetails());
		
		boolean status = Point2D.isEqual(p1,p2);
		if(status == true)
			System.out.println("Points are same");
		else
			System.out.println("Points are different");
		
		System.out.println("The distance between two points is "+Point2D.calculateDistance(p1,p2));
		
	}

}
