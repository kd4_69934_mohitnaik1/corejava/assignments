package test;

import java.util.Scanner;

import mmnaik.electronics.ElectronicStock;
import mmnaik.electronics.Electronics;
import mmnaik.electronics.Mobile;
import mmnaik.electronics.Tv;
import mmnaik.electronics.WashingMachine;
import mmnaik.exception.ElectronicsException;

public class Test {

	public Test() {
		// TODO Auto-generated constructor stub
	}
	
public static int menu(Scanner sc) {
		
		System.out.println("1. Add Products");
		System.out.println("2. Display");
		System.out.println("3. Purchase");
		System.out.println("0. Exit");
		return sc.nextInt();
	}
	
	public static int menu2() {
	
	System.out.println("1. Purchase Mobile");
	System.out.println("2. Purchase Tv");
	System.out.println("3. Purchase Washing Machine");
	System.out.println("0. Exit");
	return new Scanner(System.in).nextInt();
}

	
	public static void purchaseProduct(ElectronicStock Es[],Scanner sc) {
		int choice = menu2();
		
		while(choice!=0) {
			
			switch (choice) {
			case 1:
				Es[0].purchaseProduct(sc);
				break;
			case 2:
				Es[1].purchaseProduct(sc);
				break;
			case 3:
				Es[2].purchaseProduct(sc);
			default:
				break;
			}
			choice = menu2();
		}
	}
	
	public static void main(String[] args) throws ElectronicsException {
		// TODO Auto-generated method stub
		ElectronicStock[] Es = new ElectronicStock[3];
		Es[0] = new ElectronicStock();
		Es[1] = new ElectronicStock();
		Es[2] = new ElectronicStock();
		
		Scanner sc = new Scanner(System.in);
		int choice = menu(sc);
		
		
		while(choice != 0) {
			
			switch (choice) {
			case 1:
				System.out.println("Enter for Mobile:");
				Es[0].addStock(new Mobile(),sc);
				System.out.println("Enter for Tv:");
				Es[1].addStock(new Tv(),sc);
				System.out.println("Enter for WashingMachine1:");
				Es[2].addStock(new WashingMachine(),sc);
				break;
				
			case 2:
				Es[0].displayStock();
				Es[1].displayStock();
				Es[2].displayStock();
				break;
			
			case 3:
				purchaseProduct(Es, sc);
				break;

			default:
				break;
			}
			
			choice = menu(sc);
		}
		
	}

}
