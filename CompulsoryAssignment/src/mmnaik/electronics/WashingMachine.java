package mmnaik.electronics;

import java.util.Scanner;

import mmnaik.exception.ElectronicsException;

public class WashingMachine extends Electronics{
	
	int capacity;
	String type; // semi/full automatic
	// override acceptdata and printdata
	// call super class accept and print method in it
	
	public WashingMachine() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public
	void acceptData(Scanner sc) throws ElectronicsException {

		accept(sc);
		System.out.println("Enter capacity");
		int capacity = sc.nextInt();
		if(capacity<=0)
			throw new ElectronicsException("Capacity cannot be <= 0");
		else
			this.capacity = capacity;
		System.out.println("Enter type: semi/auto");
		String s = sc.next();
		while(s=="semi" || s=="auto") {
			System.out.println("Wrong input enter semi/auto");
			s = sc.next();
		}
		this.type = s;
		
	}
	@Override
	public
	void printData() {
		// TODO Auto-generated method stub
		super.print();
		System.out.println("Capacity is "+this.capacity+". Stroage is "+this.type);
		
	}

	@Override
	public void correctData(Scanner sc) {
		int capacity=0;
		while(capacity<=0) {
			System.out.println("Enter capacity:");
				capacity = sc.nextInt();
			}
		this.capacity =capacity;
		System.out.println("Enter type: semi/auto");
		String s = sc.next();
		while(s=="semi" || s=="auto") {
			System.out.println("Wrong input enter semi/auto");
			s = sc.next();
	}
		this.type = s;

}
}
