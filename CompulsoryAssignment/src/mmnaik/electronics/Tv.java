package mmnaik.electronics;

import java.util.Scanner;

import mmnaik.exception.ElectronicsException;

public class Tv extends Electronics{
	
	int screen_inches;
	int pixel_density;
	// override acceptdata and printdata
	// call super class accept and print method in it
	public Tv() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public
	void acceptData(Scanner sc) throws ElectronicsException {

		accept(sc);
		System.out.println("Enter screen_inches");
		int screen_inches = sc.nextInt();
		if(screen_inches <=0)
			throw new ElectronicsException("Screen size can't be 0");
		else
			this.screen_inches = screen_inches;
		System.out.println("Enter pixel density");
		int pixel_density = sc.nextInt();
		if(pixel_density<=0)
			throw new ElectronicsException("Pixel density can't be 0");
		this.pixel_density = pixel_density;
		
		
	}
	@Override
	public
	void printData() {

		super.print();
		System.out.println("Screen size is "+this.screen_inches+". Pixel density is "+this.pixel_density);
		
	}
	@Override
	public void correctData(Scanner sc) {
		int screen_inches = 0;
		while(screen_inches<=0) {
			System.out.println("Enter Screen size:");
				screen_inches = sc.nextInt();
			}
		this.screen_inches = screen_inches;
		int pixel_density = 0;
			while(pixel_density <=0 ) {
				System.out.println("Enter Pixel density:");
				pixel_density = sc.nextInt();
			}
			this.pixel_density = pixel_density;
	}
	
//	@Override
//	public void correctData(Scanner sc) {
//		// TODO Auto-generated method stub
//		
//	}

}
