package mmnaik.electronics;

import java.util.Scanner;

import mmnaik.exception.ElectronicsException;
import mmnaik.exception.MobileException;

abstract public class Electronics {
	String model;
	String description;
	double price;
	
	// to accept electronics field data
	void accept(Scanner sc) {
		System.out.println("Enter Model name");
		this.model = sc.next();
		System.out.println("Enter Description");
		this.description = sc.next();
		System.out.println("Enter price");
		this.price = sc.nextDouble();
	}
	
	public abstract void acceptData(Scanner sc) throws ElectronicsException,MobileException;
	// to print electronics field data
	
	void print() {
		System.out.println("Model: "+this.model+" Description: "+this.description+" Price: "+
				this.price);
	}
	public abstract void correctData(Scanner sc);
	public abstract void printData();


}
