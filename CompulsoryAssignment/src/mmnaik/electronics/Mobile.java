package mmnaik.electronics;

import java.util.Scanner;

import mmnaik.exception.MobileException;

public class Mobile extends Electronics{
	int ram;
	int storage;
	// override acceptdata and printdata
	// call super class accept and print method in it
	@Override
	public void printData() {
		super.print();
		System.out.println("Ram is "+this.ram+"GB. Stroage is "+this.storage);
	}
		
	public Mobile() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Mobile(int ram, int storage) {
		super();
		this.ram = ram;
		this.storage = storage;
	}

	@Override
	public void acceptData(Scanner sc) throws MobileException{
		
		accept(sc);
		System.out.println("Enter ram");
		int ram1 = sc.nextInt();
		if(ram1<=0)
			throw new MobileException("Ram cannot be <= 0");
		else
			this.ram = ram1;
		System.out.println("Enter storage");
		int storage1 = sc.nextInt();
		if(storage1<=0)
			throw new MobileException("Storage cannot be <= 0");
		else
			this.storage = storage1;

	}
	

	public void correctData(Scanner sc) {
		while(this.ram<=0) {
		System.out.println("Enter ram:");
			this.ram = sc.nextInt();
		}
		int storage = 0;
		while(storage <=0 ) {
			System.out.println("Enter Storage:");
			storage = sc.nextInt();
		}
		this.storage=storage;		
	}
	

	public int getStorage() {
		return this.storage;
	}

}
