package mmnaik.electronics;

import java.util.Scanner;

import mmnaik.exception.ElectronicsException;
import mmnaik.exception.MobileException;
import mmnaik.stockManager.StockManager;

public class ElectronicStock implements StockManager{
	
	Electronics item;
	int quantity;
	
	public ElectronicStock() {
		// TODO Auto-generated constructor stub
	}


	@Override
	public void purchaseProduct(Scanner sc) {
		// TODO Auto-generated method stub
//		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Quantity to purchase: ");
		int qty = sc.nextInt();
		if(qty<=0) {
			System.out.println("Can't buy neagtive 0 or negative qty...");
		}
		else if(qty>this.quantity)
			System.out.println("Only "+this.quantity+" available.");
		else
			this.quantity-=qty;
		
		
	}

	@Override
	public void displayStock() {
		// TODO Auto-generated method stub
		System.out.println("************************");
		this.item.printData();
		System.out.println("Quantity : "+ this.quantity);
		System.out.println("*************************");
	}
	

	@Override
	public void addStock(Electronics item,Scanner sc) {
		try {
			
			item.acceptData(sc);

			
		} catch (MobileException e) {
			{
				System.out.println(e);
				e.printStackTrace();
				Mobile m = (Mobile)item;
				m.correctData(sc);
//				this.quantity = sc.nextInt();
//				this.item = m;
			}
			
			
		} catch (ElectronicsException e) {
			System.out.println(e);
			e.printStackTrace();
//			System.exit(0);
			Tv t; WashingMachine wm;
			if(item instanceof Tv) {
				t = (Tv)item;
				t.correctData(sc);
			}
				
			else {
				wm = (WashingMachine)item;
				wm.correctData(sc);
			}
				
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
//			System.exit(0);
		}
		
		System.out.println("Enter Quantity : ");
		this.quantity=sc.nextInt();
		this.item = item;
		


	}

}
