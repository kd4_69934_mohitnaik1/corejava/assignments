package mmnaik.exception;

import mmnaik.electronics.Tv;
import mmnaik.electronics.WashingMachine;
import test.Test;

public class ElectronicsException extends Exception{
	String message;
	public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    
	public ElectronicsException() {
		// TODO Auto-generated constructor stub
	}
	
	public ElectronicsException(String message) {
		// TODO Auto-generated constructor stub
		this.message = message;
	}

	@Override
	public void printStackTrace() {
		// TODO Auto-generated method stub
		System.out.println(ANSI_RED+Test.class+ANSI_RESET);
		if(this.message.contains("capacity"))
			System.out.println(ANSI_RED+WashingMachine.class+ANSI_RESET);
		else 
		System.out.println(ANSI_RED+Tv.class+ANSI_RESET);
		
		System.out.println(ANSI_RED+"Error Message  : "+this.message+ANSI_RESET);
	}

}
