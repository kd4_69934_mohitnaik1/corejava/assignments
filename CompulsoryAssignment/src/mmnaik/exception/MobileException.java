package mmnaik.exception;

import mmnaik.electronics.Electronics;
import mmnaik.electronics.Mobile;
import mmnaik.electronics.Tv;
import mmnaik.electronics.WashingMachine;
import test.Test;

public class MobileException extends Exception{
	String message;
	public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    
	public MobileException() {
	}
	
public MobileException(String message) {
		this.message = message;
	}
	
	@Override
	public void printStackTrace(){
		// TODO Auto-generated method stub
//		
		System.out.println(ANSI_RED + Test.class + ANSI_RESET);
		if(this.message.contains("Ram")) {
			System.out.println(ANSI_RED + Mobile.class+ANSI_RESET);
		}
		System.out.println(ANSI_RED +"Error Message  : "+this.message+ANSI_RESET);
	}
}
