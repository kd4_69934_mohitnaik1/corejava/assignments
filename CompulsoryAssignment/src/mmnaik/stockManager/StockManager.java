package mmnaik.stockManager;

import java.util.Scanner;

import mmnaik.electronics.Electronics;
import mmnaik.electronics.Mobile;
import mmnaik.electronics.Tv;
import mmnaik.electronics.WashingMachine;
import mmnaik.exception.MobileException;

public interface StockManager {
	
	void purchaseProduct(Scanner sc);
	void displayStock();
	void addStock(Electronics m,Scanner sc) throws MobileException;
	
}
