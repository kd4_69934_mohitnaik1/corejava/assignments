package tester;

import java.util.Scanner;
import com.app.geometry.Point2D;

public class TestPointArray1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of points");
		int num = sc.nextInt();
		
		Point2D[] points = new Point2D[num];
		
		for(int i=0;i<num;i++) {
			System.out.println("Enter x and y for point"+(i+1));
			int x = sc.nextInt();
			int y = sc.nextInt();
			points[i] = new Point2D(x,y);
		}
			
			System.out.println("1. Display details of a point");
			System.out.println("2. Display x,y for all points");
			System.out.println("3. Display distance between two points");
			System.out.println("0. Exit");
			System.out.println("Enter a choice");
			int choice = sc.nextInt();
			
		switch(choice) {
		
				case 1:
					System.out.println("Enter index no. of a point");
					int i = sc.nextInt();
					while(i<0 || i>=num) {
						System.out.println("Invalid index, pls retry!!!");
						i = sc.nextInt();
					}
					System.out.println(points[i].getDetails());
					break;
					
				case 2:
					for(Point2D point : points) {
						point.getDetails();
					}
					break;
					
				case 3:
					System.out.println("Enter index no. of a point1");
					int p1 = sc.nextInt();
					System.out.println("Enter index no. of a point2");
					int p2 = sc.nextInt();
					
					if(Point2D.isEqual(points[p1], points[p2]))
						System.out.println("Both points are same");
					else
						System.out.println("Distance between two points is "+Point2D.calculateDistance(points[p1], points[p2]));
					break;
					
				default:
					System.out.println("Wrong input. Choose from above options");
		}	
		
}
}
