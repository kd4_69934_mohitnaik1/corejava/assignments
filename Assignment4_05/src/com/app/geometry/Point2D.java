package com.app.geometry;

public class Point2D {
	private int x;
	private int y;
	
	public Point2D(int x,int y){
		this.x = x;
		this.y = y;
	}
	
	//getters
	public int getX() {
		return this.x;
	}
	public int getY() {
		return this.y;
	}
	
	public String getDetails() {
		return "X co-ordinate ="+x+" Y co-ordinate ="+y;
	}
	
	public static boolean isEqual(Point2D p1,Point2D p2) {
		if((p1.x == p2.x) && (p1.y == p2.y))
			return true;
		return false;
	}

	public static double calculateDistance(Point2D p1,Point2D p2) {
		double diffX = p1.getX() - p2.getX();
		double diffY = p1.getY() - p2.getY();
		
		double ans = Math.sqrt(Math.pow(diffX,2)+Math.pow(diffY,2));
		return ans;
	}
}

