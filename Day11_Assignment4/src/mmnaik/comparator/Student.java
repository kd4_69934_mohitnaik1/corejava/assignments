package mmnaik.comparator;

import java.util.Arrays;
import java.util.Comparator;


class customComp implements Comparator<Student>{

	@Override
	public int compare(Student s1, Student s2) {
		int diff = s1.getCity().compareTo(s2.getCity());
		if(diff == 0)
		{
			if(s1.getMarks()>s2.getMarks())
				return -1;
			if(s1.getMarks()<s2.getMarks())
				return +1;
			else
			{
				diff = s1.getName().compareTo(s2.getName());
				return diff;
			}
				
		}
		return -diff;
	}
	
}


public class Student {
	private int roll;
	private String name;
	private String city;
	private double marks;
	
	public Student() {
		// TODO Auto-generated constructor stub
	}
	
	public Student(int roll,String name,String city,double marks) {
		this.roll = roll;
		this.city = city;
		this.marks = marks;
		this.name = name;
	}

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public double getMarks() {
		return marks;
	}



	public void setMarks(double marks) {
		this.marks = marks;
	}
	
	@Override
	public String toString() {
		return "Student [roll=" + roll + ", name=" + name + ", city=" + city + ", marks=" + marks + "]";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Student[] s = {new Student(10,"Shiv","Pune",80),
				new Student(10,"Aditya","Pune",80),
				new Student(11,"Mohit","Pune",74),
				new Student(11,"Ritesh","Ahmednagar",71.69),
				new Student(14,"Ritesh","Sambhajinagar",80.5),
				new Student(15,"Chetan","Sambhajinagar",80.92),
				new Student(16,"Rohit","Mumbai",91),
				new Student(18,"Gauresh","Karad",81.92),
				};
		
		for (Student student : s) {
			System.out.println(student);
		}
		Arrays.sort(s,new customComp());
		System.out.println();
		for (Student student : s) {
			System.out.println(student);
		}
	}

}
