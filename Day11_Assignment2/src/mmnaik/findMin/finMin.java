package mmnaik.findMin;

import java.util.Scanner;

public class finMin {

	public static <T extends Number> T finMini(T[] arr) {
		T min = arr[0];
		for (T t : arr) {
			if(t.doubleValue() < min.doubleValue())
				min = t;
		}
		return min;
	}
	
	public static void main(String []args) {
		
		Scanner sc = new Scanner(System.in);
		Integer[] arr = new Integer[5];
		System.out.println("For Integer array");
		for(int i=0;i<5;i++)
			arr[i] = sc.nextInt();
		System.out.println("Min value is: "+finMin.finMini(arr));
		
		Float[] arr2 = new Float[5];
		System.out.println("\r\nFor Float array");
		for(int i=0;i<5;i++)
			arr2[i] = sc.nextFloat();
		System.out.println("Min value is: "+finMin.finMini(arr2));
	}

}
