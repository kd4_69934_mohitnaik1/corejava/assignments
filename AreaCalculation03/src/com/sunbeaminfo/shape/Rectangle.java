package com.sunbeaminfo.shape;

import java.util.Scanner;

public class Rectangle extends Shape {
	private int length;
	private int breadth;

	public Rectangle() {
		this(0, 0);
	}

	public Rectangle(int length, int breadth) {
		this.length = length;
		this.breadth = breadth;
	}

	@Override
	public double claculateArea() {
		return this.area = this.length * this.breadth;

	}

	@Override
	public String toString() {
		return "Area of Rectangle = " + this.area;
	}

	@Override
	public void accceptRecord() {
		System.out.println("Enter length and breadth = ");
		Scanner sc = new Scanner(System.in);
		this.length = sc.nextInt();
		this.breadth = sc.nextInt();

	}

}
