package com.sunbeaminfo.shape;

import java.util.Scanner;

public class Square extends Shape {
	private int side;

	public Square() {
		this.side = 0;
	}

	public Square(int side) {
		this.side = side;
	}

	@Override
	public double claculateArea() {
		return this.area = this.side * this.side;

	}

	@Override
	public String toString() {
		return "Area of Square = " + this.area;
	}

	@Override
	public void accceptRecord() {
		System.out.println("Enter side = ");
		this.side = new Scanner(System.in).nextInt();

	}

}
