package com.sunbeaminfo.shape;

public abstract class Shape {
	protected double area;

	public Shape() {
		this.area = 0;
	}

	public abstract double claculateArea();

	public abstract void accceptRecord();

	public double getArea() {
		return area;
	}

}
