package com.sunbeaminfo.shape;

import java.util.Scanner;

public class Circle extends Shape {
	private int radius;

	public Circle() {
		this.radius = 0;
	}

	@Override
	public double claculateArea() {
		return this.area = 3.14 * this.radius * this.radius;
	}

	@Override
	public void accceptRecord() {
		System.out.println("Enter Radius = ");
		this.radius = new Scanner(System.in).nextInt();
	}

}
