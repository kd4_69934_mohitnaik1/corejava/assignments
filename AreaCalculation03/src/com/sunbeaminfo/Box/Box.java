package com.sunbeaminfo.Box;

import com.sunbeaminfo.shape.Shape;

public class Box<T extends Shape>{
	private T obj;
	
	public Box() {
		
	}
	
	public Box(T ob) {
		this.obj = ob;
	}
	
	public T getObj() {
		return this.obj;
	}
	
	public void setObj(T ob) {
		this.obj = ob;
	}
	
	public double display() {
		return this.obj.claculateArea();
	}
	

}
