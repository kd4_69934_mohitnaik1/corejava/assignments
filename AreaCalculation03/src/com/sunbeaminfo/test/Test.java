package com.sunbeaminfo.test;

import java.util.Scanner;

import com.sunbeaminfo.Box.Box;
import com.sunbeaminfo.shape.Circle;
import com.sunbeaminfo.shape.Rectangle;
import com.sunbeaminfo.shape.Shape;
import com.sunbeaminfo.shape.Square;

public class Test {
	private static int menu() {
		System.out.println("1. Area of Rectangle");
		System.out.println("2. Area of Square");
		System.out.println("3. Area of Circle");
		System.out.println("Enter your choice = ");
		return new Scanner(System.in).nextInt();
	}

	public static void main(String[] args) {
//		Shape s[] = new Shape[3];
//		for (int i = 0; i < 3; i++) {
//			int choice = menu();
//			switch (choice) {
//			case 1:
//				s[i] = new Rectangle();
//				break;
//			case 2:
//				s[i] = new Square();
//				break;
//			case 3:
//				s[i] = new Circle();
//				break;
//			}
//			if (s[i] != null) {
//				s[i].accceptRecord();
//				s[i].claculateArea();
//			} else
//				i--;
//		}
//
//		for (Shape shape : s) {
//			System.out.println(shape);
//		}
		Box<Circle> c = new Box<>();
		c.setObj(new Circle());
		c.getObj().accceptRecord();
		System.out.println(c.display());	
		
		Box<Rectangle> r = new Box<>();
		r.setObj(new Rectangle());
		r.getObj().accceptRecord();
		System.out.println(r.display());
		
		Box<Square> s = new Box<>();
		s.setObj(new Square());
		s.getObj().accceptRecord();
		System.out.println(s.display());
	}

}
//
//
///
//
//
//
