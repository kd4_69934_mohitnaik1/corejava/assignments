package question1;

class Date {
	private int month;
	private int day;
	private int year;

	public Date(int month,int day,int year) {
		this.month = month;
		this.day = day;
		this.year = year;
	}
	
	//setters
	void setMonth(int month)
	{
		this.month = month;
	}
	void setDay(int day)
	{
		this.day = day;
	}
	void setYear(int year)
	{
		this.year = year;
	}
	
	//getters
	int getYear()
	{
		return this.year;
	}
	int getMonth()
	{
		return this.month;
	}
	int getDay()
	{
		return this.day;
	}
	
	void displayDate() {
		System.out.print("The date is ");
		System.out.println(this.month+"/"+this.day+"/"+this.year);
	}
	
}
