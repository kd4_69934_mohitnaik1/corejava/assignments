package mmnaik.assignment2;

import java.util.function.Predicate;

public class CountNoOfStr {
	
	
	public static int countIf(String[] arr,Predicate<String> cond) {
		int count = 0;
		for(String str:arr) {
			if(cond.test(str))
				count++;
		}
		return count;
	}
	
	public static void main(String[] args) {
		
		String[] arr = { "Nilesh", "Shubham", "Pratik", "Omkar", "Prashant" };
		
		//with anonymous class object 
		int res = countIf(arr,new Predicate<String>(){
			
			@Override
			public boolean test(String t) {
				if(t.length()>6)
					return true;
				return false;
			}
		});
		
		System.out.println(res);
		
		// with lambda expressions
		res = countIf(arr,(t) -> t.length()>6);
		
		System.out.println(res);
		
	}

}
