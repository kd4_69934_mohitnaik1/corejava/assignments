import java.util.Scanner;

public class PrimesInRange {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter num1 ");
		int num1 = sc.nextInt();
		System.out.println("Enter num2 greater than num1");
		int num2 = sc.nextInt();
		boolean status = false;
		
		while(num2<=num1)
		{
			System.out.println("Please enter number greater than "+num1);
			num2 = sc.nextInt();
		}
			
		for(int number=num1;number<=num2;number++)
		{
			for(int i=2;i<number;i++)
			{
				if(number%i==0)
				{
					status = true;
					break;
				}
			}
			
			if(!status)
				System.out.print(number+" ");
			status = false;
		}
		
		sc.close();
	}

}
