import java.util.Scanner;

public class pattern2 
{
	public static void main(String []args) {
		
		Scanner sc = new Scanner(System.in);
		
		int rows = sc.nextInt();
		
		for(int i=1;i<=rows;i++)
		{
			for(int j=0;j<rows-i;j++)
			{
				System.out.print(" ");
			}
			for(int j=rows-i;j<rows;j++)
			{
				System.out.print("*");
			}
			System.out.println("");
		}
		sc.close();
	}
}
